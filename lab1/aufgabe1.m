function aufgabe1()
%alpha = 'abcdefghijklmnopqrstuvwxyz';
%Computer:
%fid = fopen('C:\Users\siegfried\Desktop\Kommunikationstechnik\Uebung_Neu\Labor\Labor_1\rfc2795.txt', 'r');
%Laptop:
fid = fopen('C:\Users\siegfried\Desktop\Studium\Kommunikationstechnik\Labor\rfc2795.txt', 'r');
A = fread(fid);
%{
tf = ismember(lower(A),alpha); -- schauen ob Zeichen in A in alpha
vorhanden, wenn ja schreibe eine 1 in vector ansonsten 0
A(tf(:,1)==0,:)=[];           -- wenn in Vector tf == 0, dann in A = [] (Vorraussetzung beide vectoren gleich gro�) 
%}
fclose(fid);
characters = sort(char(A));
characters(characters == ' ') = [];
characters(double(characters) == 10) = [];
uniqueCharacters = unique(double(characters));
countElements = histc(characters, uniqueCharacters); % histc z�hlt die Werte in characters 
plot(sort(countElements, 1,'descend'), 'Color', [0,1,0])
set(gca, 'fontname', 'arial', 'fontsize', 16);
xlabel('Zeichen (sortiert)');
ylabel('H�ufigkeit');