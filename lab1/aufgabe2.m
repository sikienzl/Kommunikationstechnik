function aufgabe2()
%Computer:
%fid = fopen('C:\Users\siegfried\Desktop\Kommunikationstechnik\Uebung_Neu\Labor\Labor_1\rfc2795.txt', 'r');
%Laptop:
fid = fopen('C:\Users\siegfried\Desktop\Studium\Kommunikationstechnik\Labor\rfc2795.txt', 'r');
A = fread(fid);
fclose(fid);
characters = sort(char(A));
characters(characters == ' ') = [];
characters(double(characters) == 10) = [];
uniqueCharacters = unique(double(characters));
elementsWithCommonness = [uniqueCharacters, histc(characters, uniqueCharacters)];
sortedElementsWithCommonness = sortrows(elementsWithCommonness, 2, 'descend');
str = char(sortedElementsWithCommonness(1:10,1));
y2 = sortedElementsWithCommonness(1:10,2);
bar(y2, 'FaceColor', [1,1,0]);
set(gca, 'XTickLabel', str , 'XTick', 1:numel(str));
set(gca, 'fontname', 'arial', 'fontsize', 16 );
xlabel('Zeichen');
ylabel('Häufigkeit');

