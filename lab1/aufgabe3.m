function aufgabe3()
%Computer:
%fid = fopen('C:\Users\siegfried\Desktop\Kommunikationstechnik\Uebung_Neu\Labor\Labor_1\rfc2795.txt', 'r');
%Laptop:
fid = fopen('C:\Users\siegfried\Desktop\Studium\Kommunikationstechnik\Labor\rfc2795.txt', 'r');
A = fread(fid);
fclose(fid);
characters = sort(char(A));
characters(characters == ' ') = [];
characters(double(characters) == 10) = [];
uniqueCharacters = unique(double(characters));
%Auftrittswahrscheinlichkeit:
occursProbability = [uniqueCharacters, (histc(characters, uniqueCharacters) / length(characters))]; % Berechnung Auftrittswahrscheinlichkeit
%occursProbabilitySorted = sortrows(occursProbability, 2, 'descend');

%str = char(occursProbabilitySorted(1:length(occursProbabilitySorted),1));
%y2 = occursProbabilitySorted(1:length(occursProbabilitySorted),2);

%plot(y2, 'Color', [0,1,0])
%set(gca, 'XTickLabel', str , 'XTick', 1:numel(str));
%set(gca, 'fontname', 'arial', 'fontsize', 16);
%xlabel('Zeichen (sortiert)');
%ylabel('Auftrittswahrscheinlichkeit');

%Informationsgehalt:
informationContent = [uniqueCharacters, log2(1./occursProbability(:,2))];

informationContentSorted = sortrows(informationContent, 2, 'descend');

str = char(informationContentSorted(:,1));

informationContentSorted2 = informationContentSorted(:, 2);

plot(informationContentSorted2, 'Color', [0,1,0])
set(gca, 'XTickLabel', str , 'XTick', 1:numel(str));
set(gca, 'fontname', 'arial', 'fontsize', 16);
xlabel('Zeichen (sortiert)');
ylabel('Informationsgehalt');
