function aufgabe4()
%Computer:
%fid = fopen('C:\Users\siegfried\Desktop\Kommunikationstechnik\Uebung_Neu\Labor\Labor_1\rfc2795.txt', 'r');
%Laptop:
fid = fopen('C:\Users\siegfried\Desktop\Studium\Kommunikationstechnik\Labor\rfc2795.txt', 'r');
A = fread(fid);
fclose(fid);
characters = sort(char(A));
characters(characters == ' ') = [];
characters(double(characters) == 10) = [];
uniqueCharacters = unique(double(characters));
%Auftrittswahrscheinlichkeit:
occursProbability = [uniqueCharacters, (histc(characters, uniqueCharacters) / length(characters))]; % Berechnung Auftrittswahrscheinlichkeit


%Informationsgehalt:
informationContent = [uniqueCharacters, log2(1./occursProbability(:,2))];

informationContentSorted = sortrows(informationContent, 2, 'descend');

%Entropie:
entropyOfCharacters = occursProbability(:, 2) .* log2(length(characters)./histc(characters, uniqueCharacters));

sumOfentropy = sum(entropyOfCharacters);

disp(sumOfentropy);

%Mittlere Codewortlänge einer Nachricht - 88 Zeichen --> 7 Bits 

lc = sum(occursProbability(:,2) .* 7);

disp(lc);

% Redundanz einer Codierung:

rc = lc - sumOfentropy;

disp(rc);



