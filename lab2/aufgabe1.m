function aufgabe1()
str = 'HOCHSCHULE';

characters = char(str);
uniqueCharacters = unique(double(characters));
%Auftrittswahrscheinlichkeit:
occursProbability = [uniqueCharacters; (histc(characters, uniqueCharacters) / length(characters))]; % Berechnung Auftrittswahrscheinlichkeit

disp('Auftrittswahrscheinlichkeiten:');

disp(occursProbability);

%Informationsgehalt:
informationContent = [uniqueCharacters; log2(1./occursProbability(2,:))];

informationContentSorted = sortrows(informationContent, 2, 'descend');

%Entropie:
entropyOfCharacters = occursProbability(2, :) .* log2(length(characters)./histc(characters, uniqueCharacters));

sumOfentropy = sum(entropyOfCharacters);

disp('Entropie der Nachrichtenquelle:');

disp(sumOfentropy);


